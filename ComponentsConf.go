package cServer

import "gitee.com/csingo/cComponents"

const ComponentsConfigName = "ComponentsConf"

type ComponentsConf struct {
	Components []cComponents.ComponentInterface
}

func (i *ComponentsConf) ConfigName() string {
	return ComponentsConfigName
}

var components_config *ComponentsConf
