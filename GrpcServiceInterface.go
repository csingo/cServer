package cServer

import "google.golang.org/grpc"

type GrpcServiceInterface interface {
	GrpcServiceDesc() *grpc.ServiceDesc
}
