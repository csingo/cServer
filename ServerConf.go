package cServer

const ServerConfigName = "ServerConf"

type ServerConf struct {
	ServerName string                `json:"server_name"`
	HttpServer ServerConf_HttpServer `json:"http_server"`
	GrpcServer ServerConf_GrpcServer `json:"grpc_server"`
}

type ServerConf_HttpServer struct {
	Enable          bool                        `json:"enable"`            // 是否启用 http 服务
	Debug           bool                        `json:"debug"`             // 是否启用 pprof 工具
	Port            int                         `json:"port"`              // http 端口号
	ReadTimeout     int                         `json:"read_timeout"`      // 请求内容读取超时时间
	WriteTimeout    int                         `json:"write_timeout"`     // 返回内容写入超时时间
	IdleTimeout     int                         `json:"idle_timeout"`      // 在启用 keep-alive 的情况下，等待下一个请求的最大空闲时间。如果为零，则使用 ReadTimeout 的值
	MaxHeaderBytes  int                         `json:"max_header_bytes"`  // 请求头大小
	BasicAuthEnable bool                        `json:"basic_auth_enable"` // 是佛开启基础权限校验
	BasicAuthUsers  map[string]string           `json:"basic_auth_users"`  // 基础权限账号密码
	Cross           ServerConf_HttpServer_Cross `json:"cross"`             // 跨域设置
}

type ServerConf_HttpServer_Cross struct {
	Enable           bool     `json:"enable"`
	AllowOrigins     []string `json:"allow_origins"`
	AllowMethods     []string `json:"allow_methods"`
	AllowHeaders     []string `json:"allow_headers"`
	AllowCredentials bool     `json:"allow_credentials"`
	AllowWebSockets  bool     `json:"allow_websockets"`
	MaxAge           int64    `json:"max_age"`
}

type ServerConf_GrpcServer struct {
	Enable bool `json:"enable"` // 是否启用 grpc 服务
	Port   int  `json:"port"`   // grpc 服务端 端口
}

func (i *ServerConf) ConfigName() string {
	return ServerConfigName
}

var server_config *ServerConf
