package cServer

const RouteConfigName = "RouteConf"

type RouteType int

const (
	RouteTypeNil RouteType = iota
	RouteTypeApi
	RouteTypeFile
	RouteTypeDir
	RouteTypeView
	RouteTypeGrpc
	RouteTypeGroup
)

type RouteConf struct {
	Type        RouteType              `json:"type"`
	Path        string                 `json:"path"`
	Method      []string               `json:"method"`
	Target      string                 `json:"target"`
	App         string                 `json:"app"`
	Controller  string                 `json:"controller"`
	Function    string                 `json:"function"`
	Middlewares []RouteConf_Middleware `json:"middlewares"`
	Routes      []RouteConf            `json:"routes"`
}

type RouteConf_Middlewares struct {
	Prefix []RouteConf_Middleware `json:"prefix"`
	Suffix []RouteConf_Middleware `json:"suffix"`
}

type RouteConf_Middleware struct {
	App    string         `json:"app"`
	Name   string         `json:"name"`
	Params map[string]any `json:"params"`
}

func (i *RouteConf) ConfigName() string {
	return RouteConfigName
}

var route_config *RouteConf
