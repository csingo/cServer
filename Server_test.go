package cServer

// type TestConf struct {
// 	Name string
// 	Age  int64
// }
//
// func (t *TestConf) ConfigName() string {
// 	return "TestConf"
// }
//
// var test_config = &TestConf{
// 	Name: "",
// 	Age:  0,
// }
//
// var log_config = &cLog.LogConf{
// 	Level:   cLog.TraceLevel,
// 	Drivers: []cLog.LogDriver{cLog.LOG_DRIVER_STDOUT},
// }
//
// var component_config = &ComponentsConf{
// 	Components: []cComponents.ComponentInterface{
// 		cLog.Component,
// 		Component,
// 		cConfig.Component,
// 	},
// }
//
// var config_center_config = &cConfig.ConfigCenterConf{
// 	Enable:   true,
// 	Backup:   false,
// 	Driver:   cConfig.FileDriver,
// 	Interval: 1,
// 	Layers:   1000,
// 	Clients: &cConfig.ConfigCenterConf_Clients{
// 		File: &cConfig.FileClient{
// 			Namespace: "local",
// 			Path:      "C:\\Users\\Joe\\project\\golang\\csingo\\cConfig",
// 		},
// 		Nacos:     nil,
// 		Customize: nil,
// 	},
// 	Listens: []*cConfig.ConfigCenterConf_ListenItem{
// 		{
// 			Group: "default",
// 			Data:  "test",
// 			Conf:  "TestConf",
// 		},
// 	},
// }
//
// func TestServer(t *testing.T) {
// 	Inject(log_config)
// 	Inject(component_config)
// 	Inject(config_center_config)
// 	Inject(test_config)
//
// 	go func() {
// 		for {
// 			log.Println(test_config)
// 			time.Sleep(1 * time.Second)
// 		}
// 	}()
//
// 	Load()
// }
